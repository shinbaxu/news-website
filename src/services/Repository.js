import axios from 'axios';

const baseUrl = `https://my-json-server.typicode.com/quancntt05/fake-server`;

export default {
  get(url) {
    return axios.get(baseUrl + url);
  }
};
